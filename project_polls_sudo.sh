#!/usr/bin/zsh

echo "<<< Setting up project specific stuff... >>>"

# create a new user and database
sudo -u postgres zsh -c "psql -c \"create role vagrant login password 'vagrant' createdb;\""
sudo -u postgres zsh -c "psql -c \"create database pb with owner = vagrant;\""