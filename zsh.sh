#!/bin/bash

echo "<<< Installing zsh >>>"

apt-get install -y -q zsh

# permission to user vagrant for changing his shell
# This allows users of group chsh to change their shells without a password
echo "<<< Allowing user vagrant permission to change its prompt >>>"
sed -i '1s/^/auth       sufficient   pam_wheel.so trust group=chsh\n/' /etc/pam.d/chsh
groupadd chsh
usermod -a -G chsh vagrant