#!/usr/bin/zsh

echo "<<< Installing softwares >>>"

# utils
apt-get install -y -q unzip zip htop

# packages for compiling binaries
apt-get install -y -q python3-software-properties build-essential autotools-dev automake pkg-config

# legit
pip3 install legit