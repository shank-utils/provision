#!/usr/bin/zsh

echo "<<< Installing and setting up postgresql >>>"

# postgresql - the database
apt-get install -y -q postgresql postgresql-contrib

# postgresql compilation library
apt-get install -y -q libpq-dev