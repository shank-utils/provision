#!/usr/bin/zsh

echo "<<< Setting up project specific stuff... >>>"

# legit
legit install

# setup the virtualenv for the current project
echo ">>> Setup the virtualenv for the current project..."
cd /vagrant
mkvirtualenv proj
workon proj
pip install -r requirements/dev.txt

# postgresql
pip install -U psycopg2

# secret
echo "export POLLS_SECRET=aljkasoiuqweioruoiu" >> ~/.zshenv
echo "export POLLS_ENV=dev" >> ~/.zshenv
echo "export POLLS_DEV_DATABASE_URL=postgresql://vagrant:vagrant@localhost/polls" >> ~/.zshenv