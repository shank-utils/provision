#!/usr/bin/zsh

echo "Setting up project specific stuff..."

# legit
legit install

# setup the virtualenv for the current project
echo ">>> Setup the virtualenv for the current project..."
cd /vagrant
mkvirtualenv proj
workon proj
pip install -r requirements/dev.txt

# postgresql
pip install -U psycopg2

# secret
echo "export PHONEBOOK_SECRET=aljkasoiuqweioruoiu" >> ~/.zshenv
echo "export PHONEBOOK_ENV=dev" >> ~/.zshenv
echo "export RMSG_DEV_DATABASE_URL=postgresql://vagrant:vagrant@localhost/pb" >> ~/.zshenv