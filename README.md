### Intended Use
The intended use is to add this repo as a git subtree to your project directory.

### Add remote
    git remote add -f provision https://gitlab.com/shank/provision.git

### Add repo as subtree
    git subtree add --prefix provision https://gitlab.com/shank/provision.git --squash provision master

### To pull changes from remote subtree repo
	git subtree pull --prefix provision provision master

### To push changes to rempote subtree repo
	git subtree push --prefix provision provision master