#!/usr/bin/zsh

echo "<<< Setting up liquidprompt for interactive shells >>>"

# liquid prompt
echo ">>> Setting up liquidprompt"
cd && git clone https://github.com/nojhan/liquidprompt.git ~/liquidprompt
source liquidprompt/liquidprompt
echo "# Only load Liquid Prompt in interactive shells, not from a script or from scp" >> ~/.profile
echo "[[ \$- = *i* ]] && source ~/liquidprompt/liquidprompt" >> ~/.profile
mkdir -p ~/.config
cp ~/liquidprompt/liquidpromptrc-dist ~/.config/liquidpromptrc