#!/bin/bash

echo "<<< Installing git >>>"

apt-get install -y -q git
apt-get install -y -q git-extras

# handling line endings properly
git config --global core.autocrlf true