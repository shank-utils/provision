#!/usr/bin/zsh

echo "<<< Setting up virtualenvwrapper >>>"

# virtualenvwrapper
source ~/.zshrc
pip3 install --user virtualenv virtualenvwrapper
# where to store our virtualenvs
echo "export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3" >> ~/.zshenv
echo "export VIRTUALENVWRAPPER_VIRTUALENV=~/.local/bin/virtualenv" >> ~/.zshenv
mkdir -p ~/Envs
echo "export WORKON_HOME=\$HOME/Envs" >> ~/.zshenv
echo "source ~/.local/bin/virtualenvwrapper.sh" >> ~/.zshenv
source ~/.zshenv