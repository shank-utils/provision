#!/usr/bin/zsh

echo "<<< Installing Python3 and PIP >>>"

# python3 and pip3
echo ">>> Python3 and Pip3..."
apt-get install -y -q python3 python3-dev
mkdir -p tmp
cd tmp
wget https://bootstrap.pypa.io/get-pip.py
python3 get-pip.py